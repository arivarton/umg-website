from django.db import models

from saleor.order.models import OrderLine

from versatileimagefield.fields import VersatileImageField


class OrderLineExtension(models.Model):
    order_line = models.OneToOneField(OrderLine, on_delete=models.CASCADE)
    customized_image = VersatileImageField(
        upload_to='customized_images', blank=True)
