var canvas_wrapper = document.getElementById('canvas-wrapper');
var max_canvas_size = canvas_wrapper.attributes['canvas-max-size'].value;
var canvas_image = canvas_wrapper.attributes['canvas_image'].value;

// Fabric prototypes
fabric.Canvas.prototype.getItemById = function(id) {
  var object = null,
  objects = this.getObjects();
  
  for (var i = 0, len = this.size(); i < len; i++) {
    if (typeof objects[i].id == 'number' && objects[i].id == id) {
      object = objects[i];
      break;
    }
  }

  return object;
}

fabric.Object.prototype.getZIndex = function() {
    return this.canvas.getObjects().indexOf(this);
}

fabric.Canvas.prototype.getAbsoluteCoords = function(object) {
  return {
    left: object.left + this._offset.left,
    top: object.top + this._offset.top
  }
}


var canvas_settings = {
    width: max_canvas_size,
    height: max_canvas_size,
    backgroundColor: 'white',
    selectionBorderColor: '#44516c',
    selectionLineWidth: 1,
}


var canvas_front_json = $.cookie('canvas_front');
if(canvas_front_json == undefined){ 
  var canvas_front = new fabric.Canvas('canvas-front', canvas_settings);
} else {
  var canvas_front = new fabric.Canvas('canvas-front');
  canvas_front.loadFromJSON(canvas_front_json);
}

var canvas_back_json = $.cookie('canvas_back');
if(canvas_back_json == undefined){ 
  var canvas_back = new fabric.Canvas('canvas-back', canvas_settings);
} else {
  var canvas_back = new fabric.Canvas('canvas-back');
  canvas_back.loadFromJSON(canvas_back_json);
}

// Set options for canvases
var canvas_list = [canvas_front, canvas_back]
set_clippath(canvas_list);
function set_clippath(canvas_list) {
  function set_canvas_attributes(canvas_list, shape){
    for (i in canvas_list) {
      if(!(canvas_list[i].clipPath)) {
        canvas_list[i].setWidth(max_canvas_size);
        canvas_list[i].setHeight(max_canvas_size);
        // canvas_list[i].setBackgroundImage(bimg);
        canvas_list[i].clipPath = shape;
      }
    }
  }
  if(typeof shape == 'undefined') {
    fabric.loadSVGFromURL(canvas_image, function(objects, options) {
      var shape = fabric.util.groupSVGElements(objects, options); 
      if(shape.height > shape.width) {
        shape.scaleToHeight(max_canvas_size);
      } else {
        shape.scaleToWidth(max_canvas_size);
      }
      shape.left = max_canvas_size/2;
      shape.top = max_canvas_size/2;
      shape.originX = 'center'
      shape.originY = 'center'
      set_canvas_attributes(canvas_list, shape);
    });
  } else {
    set_canvas_attributes(canvas_list, shape);
  }
}


jQuery(document).ready(function($){

  layer_list = $("#LayerListModal").find('#layers');

  function show_canvas(objects){
    objects.canvas.parent().show()
    objects.canvas.siblings().show()
    objects.canvas.show()
    objects.header.show()
  }

  function hide_canvas(objects){
    objects.canvas.parent().hide()
    objects.canvas.siblings().hide()
    objects.canvas.hide()
    objects.header.hide()
  }

  function get_canvas_state(){
    var front = $( '#canvas-front' );
    var front_header = $( '#canvas-front-header' );
    var back = $( '#canvas-back' );
    var back_header = $( '#canvas-back-header' );
    if(front.css('display') == 'none'){
      return {active: {canvas: back,
                       header: back_header,
                       canvas_object: canvas_back},
              inactive: {canvas: front, 
                         header: front_header,
                         canvas_object: canvas_front}}
    } else{
      return {active: {canvas: front,
                       header: front_header,
                       canvas_object: canvas_front},
              inactive: {canvas: back, 
                         header: back_header,
                         canvas_object: canvas_back}}
    }
  }

  function change_canvas(){
    var canvas_state = get_canvas_state();
    hide_canvas(canvas_state.active);
    show_canvas(canvas_state.inactive);
  }

  function get_object_name(object){
    if(object.__proto__.type == 'textbox'){
      object_name = object.text
    }else{
      object_name = object.name
    }
    return object_name;
  }

  layer_list_buttons = `<button class="delete-layer float-right"><i class="fas fa-trash"></i></button>
                        <button class="increase-priority-layer float-right"><i class="fas fa-sort-up"></i></button>
                        <button class="decrease-priority-layer float-right"><i class="fas fa-sort-down"></i></button>`

  function render_layer_list(){
    var active_canvas = get_canvas_state().active.canvas_object
    var active_objects = active_canvas.getObjects();
    layer_list.empty();
    for (var i = 0, len = active_objects.length; i < len; i++) {
      var object = active_objects[i];
      // Sort by Z-Index
      while(object.getZIndex() != i) {
        var object = active_objects[i + 1];
      }
      layer_list.prepend('<li id="layer_list_item_' + object.id + '" fabric-id="' + object.id + '"><span>' + get_object_name(object) + '</span>' + layer_list_buttons + '</li>');
    }
    // Events for layer list
    $( '.delete-layer' ).click(function() {
      var list_item = $( this ).closest('li');
      var object = active_canvas.getItemById(list_item.attr('fabric-id'));
      active_canvas.remove(object);
      list_item.remove();
    });
    $( '.increase-priority-layer' ).click(function() {
      var list_item = $( this ).closest('li');
      var object = active_canvas.getItemById(list_item.attr('fabric-id'));
      list_item.insertBefore(list_item.prev());
      active_canvas.bringForward(object);
    });
    $( '.decrease-priority-layer' ).click(function() {
      var list_item = $( this ).closest('li');
      var object = active_canvas.getItemById(list_item.attr('fabric-id'));
      list_item.insertAfter(list_item.next());
      active_canvas.sendBackwards(object);
    });
  }  

  function positionElement(canvas, object, element) {
    var absCoords = canvas.getAbsoluteCoords(object);
    element.css({'left': (absCoords.left + element.left + element.margin) + 'px',
             'top': (absCoords.top - element.outerHeight() - element.margin) + 'px'});
  }

  function loadAndSetFont(object, font, canvas) {
    var myfont = new FontFaceObserver(font)
    myfont.load().then(function() {
      object.set("fontFamily", font);
      canvas.requestRenderAll();
    });
  }

  // Adding object to canvas
  // Handles all buttons around object
  var object_id = 0
  function add_object(object){
    var active_canvas = get_canvas_state().active.canvas_object
    var element_width = 0;
    function init_ui(element_jq){
      var element = element_jq.appendTo('.product__customizer');
      element.hide()
      element['left'] = element_width;
      element['margin'] = 3
      object.on('moving', function() { positionElement(active_canvas, object, element) });
      object.on('scaling', function() { positionElement(active_canvas, object, element) });
      object.on('selected', function() { element.show() });
      object.on('deselected', function() { element.hide() });
      positionElement(active_canvas, object, element);
      element_width = element_width + element.outerWidth() + element.margin;
      return element;
    }
    function add_font_manager(){
      var element_id = 'interactive-font-manager-' + object.id;
      var element = $('<select id="' + element_id + '" style="position:absolute"></select>');
      for (index in fonts_from_django) {
        element.append('<option value="' + fonts_from_django[index].fields.name + '">' + fonts_from_django[index].fields.name + '</option>');
      }
      element = init_ui(element);
      $(element).on('change', function() {
        loadAndSetFont(object, this.value, active_canvas);
      });
    }
    function add_font_resizer(current_font_size){
      var element_id = 'interactive-resize-font-input-' + object.id;
      input_element = init_ui($('<input size="2" value="' + current_font_size + '" type="text" id="' + element_id + '" style="position:absolute" name="font-size"></input>'));
      $(input_element).on('change', function() {
        object.set("fontSize", this.value);
        active_canvas.requestRenderAll();
      });
    }
    function add_delete_button(){
      var element_id = 'interactive-delete-object-' + object.id;
      init_ui($('<button id="' + element_id + '" style="position:absolute"><i class="fas fa-trash"></i></button>'));
    }
    object.id = object_id++;
    active_canvas.add(object);
    object.center();
    if (object.__proto__.type == 'image'){
      add_delete_button();
    } else if (object.__proto__.type == 'textbox'){
      add_font_manager();
      add_font_resizer(object.fontSize);
      add_delete_button();
    }
  }

  // First run code
  // Show active canvas
  var current_canvas_state = get_canvas_state();
  hide_canvas(current_canvas_state.inactive);
  show_canvas(current_canvas_state.active);

  // On action code
  // Rotate canvas button
  $( '#rotate-canvas' ).click(function() {
    change_canvas();
  });

  $( '#show-layers' ).click(function() {
    render_layer_list();
  });

  $( '#customize-product-link' ).click(function() {
    set_clippath(canvas_list);
    var current_canvas_state = get_canvas_state();
    $( '.product' ).hide();
    $( '.product__customizer' ).show();
    hide_canvas(current_canvas_state.inactive);
    show_canvas(current_canvas_state.active);
  });

  $( '#save-canvas' ).click(function() {
    delete canvas_front.clipPath;
    delete canvas_back.clipPath;
    $.cookie('canvas_front', JSON.stringify(canvas_front), {path: ''});
    $.cookie('canvas_back', JSON.stringify(canvas_back), {path: ''});
    $.cookie('test', 'test', {path: ''});
    $( '.product__customizer' ).hide();
    $( '.product' ).show();
  });

  $( "#add-text" ).click(function() {
    var active_canvas = get_canvas_state().active.canvas_object
    var myfont = new FontFaceObserver(fonts_from_django[0].fields.name)
    myfont.load().then(function() {
      var text = new fabric.Textbox('New text', {fontSize: 27,
                                                 fontFamily: fonts_from_django[0].fields.name,
                                                 width: active_canvas.width / 3,
                                                 lockScalingY: true,
                                                 transparentCorners: false,
                                                 cornerSize: parseFloat(6)});
      // Turn controls that are not needed off
      text['setControlVisible']('tl', 0);
      text['setControlVisible']('tr', 0);
      text['setControlVisible']('bl', 0);
      text['setControlVisible']('br', 0);
      text['setControlVisible']('mt', 0);
      text['setControlVisible']('mb', 0);
      text['setControlVisible']('mtr', 0);
      text.on('modified', function() {
        $('#layer_list_item_' + text.id + ' span').text(text.text);
      });
      add_object(text);
    });
  });

  // Image upload input change listener
  // from https://stackoverflow.com/questions/31496148/svg-in-canvas-using-fabric-js-and-filereader-api
  function handleImage(e){
    var active_canvas = get_canvas_state().active.canvas_object
    var file = e.prop('files')[0];
    var reader = new FileReader();
    reader.onload = function(event){
      if(file.type == 'image/svg'){
        fabric.loadSVGFromString(file, function(objects, options) {
          var loadedObject = fabric.util.groupSVGElements(objects, options);
          add_object(loadedObject);
        });
      }
      else{
        var img = new Image();
        img.onload = function(){
          var imgInstance = new fabric.Image(img);
          resize_scale = (active_canvas.width / imgInstance.width) / 3;
          imgInstance.set({name: file.name,
                           top: active_canvas.height/2,
                           left: active_canvas.width/2,
                           scaleY: resize_scale,
                           scaleX: resize_scale,
                           hasRotatingPoint: false,
                           transparentCorners: false,
                           cornerSize: parseFloat(6)});
          imgInstance['setControlVisible']('ml', 0);
          imgInstance['setControlVisible']('mr', 0);
          imgInstance['setControlVisible']('mt', 0);
          imgInstance['setControlVisible']('mb', 0);
          add_object(imgInstance);
        }
        img.src = event.target.result;
      }
    }
    reader.readAsDataURL(file);
  }
  $('#ImageUploadInput').on("change", function(){ 
    handleImage($(this));
  });
});
