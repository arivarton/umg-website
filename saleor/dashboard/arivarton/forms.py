from django import forms
from django.utils.translation import pgettext_lazy

from ...arivarton.models import Font

class FontForm(forms.ModelForm):
    name = forms.CharField(
        required=True,
        label=pgettext_lazy('Font name in google fonts', 'Font name'))

    class Meta:
        model = Font
        exclude = []
        labels = {
            'name': pgettext_lazy('Item name', 'Name')}
