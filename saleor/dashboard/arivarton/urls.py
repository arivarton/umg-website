from django.conf.urls import url

from . import views

urlpatterns = [
    # Product customization
    # Fonts
    url(r'^$',
        views.font_list, name='font-list'),
    url(r'^(?P<pk>[0-9]+)/$',
        views.font_details, name='font-details'),
    url(r'^add/$',
        views.font_create, name='font-add'),
    url(r'^(?P<pk>[0-9]+)/update/$',
        views.font_edit, name='font-update'),
    url(r'^(?P<pk>[0-9]+)/delete/$',
        views.font_delete, name='font-delete'),
    url(r'^bulk-update/$',
        views.font_bulk_update, name='font-bulk-update')
    ]
