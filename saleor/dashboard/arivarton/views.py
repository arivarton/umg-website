from django.contrib import messages
from django.contrib.auth.decorators import permission_required
from django.template.response import TemplateResponse
from django.utils.translation import pgettext_lazy, npgettext_lazy
from django.shortcuts import get_object_or_404, redirect
from django.views.decorators.http import require_POST

from . import forms
from ..views import staff_member_required
from ...arivarton.models import Font

@staff_member_required
@permission_required(['site.manage_settings', 'page.manage_pages'])
def font_list(request):
    fonts = Font.objects.all().order_by('name')
    ctx = {
        'fonts': fonts
    }
    return TemplateResponse(request, 'dashboard/fonts/list.html', ctx)


def font_details(request, pk):
    font = get_object_or_404(Font, pk=pk)

    ctx = {
        'font': font}
    return TemplateResponse(request, 'dashboard/fonts/detail.html', ctx)


@staff_member_required
@permission_required(['site.manage_settings', 'page.manage_pages'])
def font_create(request):
    font = Font()
    font_form = forms.FontForm(request.POST or None, instance=font)
    if font_form.is_valid():
        font.name = font.get_font_info()['family']
        font = font_form.save()
        msg = pgettext_lazy(
            'Dashboard message', 'Added font %s') % (font,)
        messages.success(request, msg)
        return redirect('dashboard:font-list')
    ctx = {
        'font_form': font_form,
        'font': font}
    return TemplateResponse(request, 'dashboard/fonts/form.html', ctx)


@staff_member_required
@permission_required(['site.manage_settings', 'page.manage_pages'])
def font_edit(request, pk):
    font = get_object_or_404(Font, pk=pk)
    form = forms.FontForm(request.POST or None, instance=font)

    if form.is_valid():
        font = form.save()
        msg = pgettext_lazy(
            'Dashboard message', 'Updated font %s') % (font,)
        messages.success(request, msg)
        return redirect('dashboard:font-details', pk=font.pk)
    ctx = {
        'font': font, 'font_form': form}
    return TemplateResponse(request, 'dashboard/fonts/form.html', ctx)


@staff_member_required
@permission_required(['site.manage_settings', 'page.manage_pages'])
def font_delete(request, pk):
    font = get_object_or_404(Font, pk=pk)
    if request.method == 'POST':
        font.delete()
        msg = pgettext_lazy(
            'Dashboard message', 'Removed font %s') % (font,)
        messages.success(request, msg)
        return redirect('dashboard:font-list')
    return TemplateResponse(
        request,
        'dashboard/fonts/modal/confirm_delete.html',
        {'font': font})


@require_POST
@staff_member_required
@permission_required(['site.manage_settings', 'page.manage_pages'])
def font_bulk_update(request):
    form = forms.FontBulkUpdate(request.POST)
    if form.is_valid():
        form.save()
        count = len(form.cleaned_data['fonts'])
        msg = npgettext_lazy(
            'Dashboard message',
            '%(count)d font has been updated',
            '%(count)d fonts have been updated',
            number='count') % {'count': count}
        messages.success(request, msg)
    return redirect('dashboard:font-list')
