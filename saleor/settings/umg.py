from .base import *


UMG_ROOT = os.path.join(PROJECT_ROOT, 'umg')


# Make this unique, and don't share it with anybody.
# DJANGO_SECRET_KEY *should* be specified in the environment. If it's not, generate an ephemeral key.
if 'SECRET_KEY' not in os.environ:
    # Create a secret key in python:
    # ''.join([random.SystemRandom().choice(string.printable) for i in range(50)])
    SECRET_KEY = '\\64SA%xq\r,Lw5#\x0cY:Ccz3n+*v8;b\t:b66.\x0b1jZq+7<q.2wJ-`V'


INSTALLED_APPS.extend(('umg.reverse',))
TEMPLATES[0]['DIRS'].insert(0, os.path.join(UMG_ROOT, 'reverse', 'templates'))
#  STATICFILES_DIRS.insert(0, ('favicons', os.path.join(UMG_ROOT, 'static', 'favicons')))
