from .umg import *

import dj_database_url

ENVIRONMENT = 'production'
DATABASES['default'] = dj_database_url.config(
    default='postgresql-rigid-84305'
)
