from django.db import models

import requests

from .validators import validate_google_font


class Font(models.Model):
    name = models.CharField(max_length=128, validators=[validate_google_font])

    def __str__(self):
        return self.name

    def get_font_info(self):
        google_fonts = requests.get('https://www.googleapis.com/webfonts/v1/webfonts?key=AIzaSyDd_62x54drzwFR8u8JmeReDKWN2E8MQvY').json()
        for i in google_fonts['items']:
            if i['family'].lower() == self.name.lower():
                return i

    def get_url(self):
        return 'https://fonts.google.com/specimen/' + self.name.replace(' ', '+')
