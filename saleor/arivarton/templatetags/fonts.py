from django import template
from ..models import Font

register = template.Library()

@register.simple_tag
def get_fonts_url():
    fonts = list(Font.objects.all())
    url = 'https://fonts.googleapis.com/css?family='
    try:
        url = url + fonts.pop().name.replace(' ', '+')
    except IndexError:
        return None
    for font in fonts:
        url = url + '|' + font.name.replace(' ', '+')
    return url
