from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _
from django.core.exceptions import ValidationError

import requests


def validate_google_font(value):
    validated = False
    google_fonts = requests.get('https://www.googleapis.com/webfonts/v1/webfonts?key=AIzaSyDd_62x54drzwFR8u8JmeReDKWN2E8MQvY').json()
    for i in google_fonts['items']:
        if i['family'].lower() == value.lower():
            validated = True
            break
    if not validated:
        raise ValidationError(
            _('%(value)s is not a valid font name. Go to https://fonts.google.com/ and use the names of the fonts you are interested in from there.'),
            params={'value': value},
        )
